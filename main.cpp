#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include "libs/utils.hpp"
#include "sprite.hpp"
#include "cardgame.hpp"

int main() {
  rand_setup();

  auto cards = load_cards("assets/cards.jpg", 13, 4);
  sf::Sprite current_card(cards.sheet, cards.map["heart"][2]);

  CardGame cardgame;
  cardgame.print();
  cardgame.place(1);
  cardgame.place(0);
  cardgame.print();

  sf::RenderWindow window(sf::VideoMode(1500, 800), "Card");
  while (window.isOpen()) {
    sf::Event event;
    while (window.pollEvent(event)) {
      if (event.type == sf::Event::Closed) window.close();
    }
    window.clear();
    window.draw(current_card);
    window.display();
  }
  return EXIT_SUCCESS;
}