Implementation of [this](https://a.pomf.cat/iihpjn.pdf) assignment.<br>
Code Style: [Google C++](https://google.github.io/styleguide/cppguide.html)

```
Sprite:
	Loads up spritesheet
	Creates rect for each suit/rank
	Then puts them into map for convenient access.
Cardgame:
	Provides core logic for cardgame.
	Holds state like current turn and who won.
	Methods for placing a card.
	Acts like a referee.

```