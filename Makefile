EXE    := card
CXX    := g++
SRC    := $(wildcard *.cpp)
OBJ    := $(SRC:.cpp=.o)
CFLAG  :=
LDFLAG := $(shell pkg-config --libs sfml-all)

all:
	$(CXX) $(SRC) -o $(EXE) $(CFLAG) $(LDFLAG)
clean:
	rm $(EXE) $(OBJ)