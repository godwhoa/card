#include "cardgame.hpp"

CardGame::CardGame() { this->restart(); }

CardGame::~CardGame() {}

// Sets random card type/id and returns it.
CardGame::card CardGame::get_random_card() {
  std::vector<std::string> suits = {"heart", "spades", "diamonds", "clubs"};
  card r_card;
  r_card.suit = suits[rand() % 4];
  r_card.rank = rand_range(1, 13);
  return r_card;
}

// Restart a game
void CardGame::restart() {
  state.won = ONGOING;
  state.current_turn = rand_range(1, 2);
  this->shuffle();
}

// Gives three random card to each player.
void CardGame::shuffle() {
  // for both player
  for (int i = 1; i < 3; i++) {
    players[i].score = 0;
    // give three cards each
    for (int j = 0; j < 3; j++) {
      players[i].inhand.push_back(get_random_card());
    }
  }
}

// Removes card from players hand.
void CardGame::place(int card_index) {
  player& m_player = players[state.current_turn];

  // Move to placed
  m_player.placed.push_back(m_player.inhand[card_index]);
  // Remove from hand
  auto iter = m_player.inhand.begin();
  m_player.inhand.erase(iter + card_index);

  // check rules
  check_rules();
  find_winner();
  // swap turns
  state.current_turn = state.current_turn == 1 ? 2 : 1;
}

// Sees if both players have took their turn and then gives score for player
// with superior card.
void CardGame::check_rules() {
  player& p1 = players[1];
  player& p2 = players[2];

  if (p1.placed.size() == p2.placed.size()) {
    if (p1.placed.back().rank > p2.placed.back().rank) {
      p1.score++;
    } else {
      p2.score++;
    }
  }
}

// Sees if any player has score of 3 to announce as winner.
void CardGame::find_winner() {
  for (int i = 1; i < 3; i++) {
    if (players[i].score == 3) {
      state.won = i == 1 ? PONE : PTWO;
    }
  }
}

void CardGame::print() {
  for (int i = 1; i < 3; i++) {
    auto& p = players[i];
    for (auto& in : p.inhand) {
      tfm::printf("Player: %d Suit: %s Rank: %d\n", i, in.suit, in.rank);
    }
  }
  tfm::printf("\n");
}