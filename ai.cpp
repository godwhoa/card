#include "ai.hpp"

AI::AI(CardGame& cg, int aid, int player_id) {
  cardgame = cg;
  id = aid;
  pid = player_id;
  winning_build();
}

AI::~AI() {}

// Makes a move with a given strategy
void AI::make_move(int strategy) {
  switch (strategy) {
    case this->HIGHEST:
      highest_first();
    case this->RANDOM:
      random();
    case this->WINNING:
      winning();
  }
}

// Places highest ranking card in hand
void AI::highest_first() {
  int i = 0;
  int highest_rank = cardgame.players[id].inhand[0].rank;
  int highest_index = 0;
  for (auto& c : cardgame.players[id].inhand) {
    if (c.rank > highest_rank) {
      highest_rank = c.rank;
      highest_index = i;
    }
    i++;
  }
  cardgame.place(highest_index);
}

// Places a card in random
void AI::random() {
  int size = cardgame.players[id].inhand.size();
  if (size > 0) {
    cardgame.place(rand_range(size));
  }
}

// Builds a map with best cards to place based on other player's cards.
void AI::winning_build() {
  // holds cards inhand sorted in ascending order
  auto ai_inhand = cardgame.players[id].inhand;
  std::sort(ai_inhand.begin(), ai_inhand.end(),
            [](auto& a, auto& b) { return a.rank < b.rank; });
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      if (ai_inhand[j].rank > cardgame.players[pid].inhand[j].rank) {
        winning_map[j] = i;
        break;
      }
    }
  }
}

// Makes best move
void AI::winning() {
  auto placed = cardgame.players[pid].placed.back();
  cardgame.place(winning_map[placed.rank]);
}