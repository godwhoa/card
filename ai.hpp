#pragma once

#include "libs/utils.hpp"
#include "cardgame.hpp"
#include <algorithm>

class AI {
 private:
  CardGame& cardgame;
  // AI id and player id
  int id;
  int pid;
  // player,ai
  std::map<int, int> winning_map;
  void highest_first();
  void random();
  void winning_build();
  void winning();

 public:
  enum { HIGHEST, RANDOM, WINNING };
  AI(CardGame& cg, int aid, int player_id);
  ~AI();
  void make_move(int strategy);
};