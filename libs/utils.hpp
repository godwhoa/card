#pragma once

#include <cstdlib>
#include <ctime>
#include "tinyformat.hpp"

// Restricts an int to a range.
inline int clamp(int value, int low, int high) {
  return value < low ? low : (value > high ? high : value);
}

// Initial seed
// Meant to be executed once at main.
inline void rand_setup() { std::srand(std::time(0)); }

// Generates random int in a range
inline int rand_range(int min, int max) {
  return rand() % (max + 1 - min) + min;
}

inline int rand_range(int max) { return rand() % max; }