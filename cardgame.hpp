#pragma once

#include <map>
#include <vector>
#include "libs/utils.hpp"

class CardGame {
 private:
  enum { PONE, PTWO, TIE, ONGOING };
  // Current game state
  struct state {
    int current_turn;  // 1 = player one and 2 = player two
    int won;           // see enums
  } state;
  // Holds suit/rank of card
  struct card {
    std::string suit;
    int rank;
  };
  // Holds score and cards in hand and placed.
  struct player {
    int score;
    std::vector<card> inhand;
    std::vector<card> placed;
  };

  card get_random_card();
  void check_rules();
  void find_winner();

 public:
  std::map<int, player> players;
  CardGame();
  ~CardGame();
  void restart();
  void shuffle();
  void place(int card_index);
  void print();
};