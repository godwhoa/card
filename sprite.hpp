#pragma once

#include <string>
#include <map>
#include <array>
#include "libs/utils.hpp"

// card_map[suit][rank] gives rect for 2 of hearts.
typedef std::map<std::string, std::map<int, sf::IntRect>> card_map;

// Holds spritesheet texture and map of card rects.
typedef struct {
  sf::Texture sheet;
  card_map map;
} cards_obj;

cards_obj load_cards(std::string path, int no_ranks, int no_suits) {
  // Load spritesheet
  cards_obj cards;
  cards.sheet.loadFromFile(path);

  // Figure out size of single card.
  sf::Vector2u size = cards.sheet.getSize();
  int card_width = size.x / no_ranks;
  int card_height = size.y / no_suits;

  std::array<std::string, 4> suits = {"heart", "spades", "diamonds", "clubs"};

  for (int y = 0; y < no_suits; y++) {
    // Each row in the spritesheet contains certain type
    std::string type = suits[clamp(y, 0, no_suits - 1)];
    for (int x = 0; x < no_ranks; x++) {
      // Insert each card into the map
      cards.map[type][x + 1] =
          sf::IntRect(x * card_width, y * card_height, card_width, card_height);
    }
  }
  return cards;
}